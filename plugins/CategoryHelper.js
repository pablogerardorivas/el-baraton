import Vue from 'vue'
import categoriesList from '../assets/categories'
import productsList from '../assets/products'

const findCategory = (id, list, self = true, child = true) => {
  let element = null
  if (id && list) {
    if (self) {
      element = list.find(item => item.id === id)
    }
    if (child) {
      let i = 0
      while (!element && i < list.length) {
        element = findCategory(id, list[i++].sublevels)
      }
    }
  }
  return element
}

const getCategory = (id, sublevel = true) => {
  return findCategory(id, categoriesList.categories, !sublevel, sublevel)
}

const getProducts = id => {
  let products = []
  if (id && productsList && productsList.products) {
    products = productsList.products.filter(item => item.sublevel_id === id)
  }
  return products
}

const CategoryHelper = {
  getCategory,
  getProducts
}

Vue.prototype.$CategoryHelper = CategoryHelper
