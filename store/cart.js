import vuexLocal from '../plugins/vuex-persist'

export const state = () => ({
  cart: []
})

export const mutations = {
  add(state, { id, qty }) {
    const index = state.cart.findIndex(product => product.id === id)
    if (index < 0) {
      state.cart.push({
        id,
        qty
      })
    } else {
      state.cart[index].qty += qty
    }
  },
  remove(state, id) {
    state.cart.splice(state.cart.findIndex(product => product.id === id), 1)
  },
  updateProductQty(state, { id, qty }) {
    const index = state.cart.findIndex(product => product.id === id)
    if (index < 0) {
      state.cart.push({
        id,
        qty
      })
    } else {
      state.cart[index].qty = qty
    }
  },
  clear(state) {
    state.cart = []
  }
}

export const actions = {
  add(state, product) {
    return new Promise((resolve, reject) => {
      state.commit('add', product)
      resolve('Producto añadido al carrito')
    })
  },
  remove(state, id) {
    state.commit('remove', id)
  },
  updateProductQty(state, product) {
    state.commit('updateProductQty', product)
  },
  clear(state) {
    state.commit('clear')
  }
}

export const plugins = [vuexLocal.plugin]
